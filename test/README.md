These are command line examples which can be used to test the library.
Simply execute the scripts with no parameters to print usage.

Example
```
./instance_get.py
```
All of the examples require the below environment variables to be set.
- EHUSER: The UUID of your API user
- EHPASS: The API key (password) of your user

Set these variables as below.
```
export EHUSER=some-value
export EHPASS=some-other-value
```
