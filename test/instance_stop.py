#!/usr/bin/env python3
# Install the dependency:
# - pip install pyeh

import argparse
import json
import os
import sys
from pyeh.main import Client

# Config
debug = False

# Arguments
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument('--debug', '-x',
                    dest='debug',
                    action='store_true',
                    help="Verbose (debug) mode",
                    default=False)
parser.add_argument('--zone', '-z', type=str,
                    dest='zone',
                    help="Zone short name",
                    default='lon-b')
parser.add_argument('uuid', type=str,
                   help='ID of instance to stop')

args = parser.parse_args()
args_dict = vars(parser.parse_args())
locals().update(args_dict)

# Check user and pass
if 'EHUSER' in os.environ and 'EHPASS' in os.environ:
  user = os.environ['EHUSER']
  pwd = os.environ['EHPASS']
else:
  print('User or password not specified')
  parser.print_usage()
  exit(1)

# Config API client
client = Client(zone=zone,
            user=user,
            pwd=pwd,
            debug=debug)

# Delete instance
instance = client.stop_instance(uuid, graceful=True)
print(json.dumps(instance, indent=2))
