#!/usr/bin/env python3
# Install the dependency:
# - pip install pyeh

import argparse
import json
import os
import sys
from pyeh.main import Client

# Config
debug = False

# Arguments
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument('--debug', '-x',
                    dest='debug',
                    action='store_true',
                    help="Verbose (debug) mode",
                    default=False)
parser.add_argument('--zone', '-z', type=str,
                    dest='zone',
                    help="Zone short name",
                    default='lon-b')
parser.add_argument('name', type=str,
                   help='Name of instance to be created')
parser.add_argument('--type', '-t', type=str,
                    dest='instancetype', metavar='TYPE',
                    help="A 'container' or 'vm'",
                    default='container')
parser.add_argument('--distro', '-l', type=str,
                    dest='distro',
                    help='Linux distro (e.g. debian9)',
                    default='ubuntu1804')
parser.add_argument('--ssh_key', '-s', type=str,
                    dest='ssh_key',
                    help='SSH key',
                    default=None)
parser.add_argument('--cpu', '-c', type=int,
                    dest='cpu', metavar='SIZE',
                    help='CPU size',
                    default=500)
parser.add_argument('--disk', '-d', type=int,
                    dest='disk', metavar='SIZE',
                    help='Disk size (GB)',
                    default=10)
parser.add_argument('--mem', '-m', type=int,
                    dest='mem', metavar='SIZE',
                    help='memory size',
                    default=512)

args = parser.parse_args()
args_dict = vars(parser.parse_args())
locals().update(args_dict)

# Check user and pass
if 'EHUSER' in os.environ and 'EHPASS' in os.environ:
  user = os.environ['EHUSER']
  pwd = os.environ['EHPASS']
else:
  print('User or password not specified')
  parser.print_usage()
  exit(1)

# Config API client
client = Client(zone=zone,
            user=user,
            pwd=pwd,
            debug=debug)

# Create instance
params = {
  'name': name,
  'type': instancetype,
  'distro': distro,
  'cpusize': cpu,
  'disksize': disk,
  'memsize': mem,
  'ssh_key': ssh_key
}
instance = client.create_instance(params)
print(json.dumps(instance, indent=2))
